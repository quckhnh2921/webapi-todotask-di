﻿namespace DI_TodoTaskAPI.DTO.UserDTO
{
    public class UserForgetPasswordDTO
    {
        public string Password { get; set; }
    }
}
