﻿namespace DI_TodoTaskAPI.DTO.UserDTO
{
    public class UserCreateDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
