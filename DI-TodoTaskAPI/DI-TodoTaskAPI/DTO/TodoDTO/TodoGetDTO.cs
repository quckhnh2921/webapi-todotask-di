﻿using DI_TodoTaskAPI.Model;

namespace DI_TodoTaskAPI.DTO.TodoDTO
{
    public class TodoGetDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsCompleted { get; set; } = false;
        public int UserID { get; set; }
    }
}
