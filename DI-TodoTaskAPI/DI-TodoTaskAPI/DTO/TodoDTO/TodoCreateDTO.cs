﻿namespace DI_TodoTaskAPI.DTO.TodoDTO
{
    public class TodoCreateDTO
    {
        public int UserID { get; set; }
        public string Content { get; set; }
    }
}
