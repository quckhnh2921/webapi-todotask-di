﻿using System.Security.Cryptography;
using System.Text;

namespace DI_TodoTaskAPI.Helper
{
    public static class EncodePassword
    {
        public static string HashPassword(string password)
        {
            using SHA256 sHA256 = SHA256.Create();
            var toByte = Encoding.UTF8.GetBytes(password);
            byte[] bytes = sHA256.ComputeHash(toByte);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public static string RandomSalt(int length = 10)
        {
            const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random random = new Random();
            char[] result = new char[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = characters[random.Next(characters.Length)];
            }
            return new string(result);
        }
    }
}
