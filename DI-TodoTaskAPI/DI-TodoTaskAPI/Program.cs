using DI_TodoTaskAPI.DataAccess;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
var sqlConnection = builder.Configuration.GetValue<string>("ConnectionString:SQLConnection");
builder.Services.AddDbContext<AppDbContext>(connection => connection.UseSqlServer(sqlConnection));  

var app = builder.Build();
app.MapControllers();
app.Run();