﻿namespace DI_TodoTaskAPI.Model
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salting { get; set; }
        public List<TodoTask> TodoTasks { get; set; }
    }
}
