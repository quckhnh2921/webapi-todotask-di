﻿namespace DI_TodoTaskAPI.Model
{
    public class TodoTask
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsCompleted { get; set; } = false;
        public User User { get; set; }
        public int UserID { get; set; }
    }
}
