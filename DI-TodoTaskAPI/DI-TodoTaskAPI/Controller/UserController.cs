﻿using DI_TodoTaskAPI.DataAccess;
using DI_TodoTaskAPI.Model;
using Microsoft.AspNetCore.Mvc;
using DI_TodoTaskAPI.DTO.UserDTO;

namespace DI_TodoTaskAPI.Controller
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _context;
        private static List<User> users = new List<User>();
        public UserController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<User> GetAll()
        {
            users = _context.Users.ToList();
            return users;
        }

        [HttpPost]
        public string CreateAccount([FromBody] UserCreateDTO userDTO)
        {
            User user = new User();
            user.UserName = userDTO.UserName;
            user.Salting = Helper.EncodePassword.RandomSalt();
            user.Password = Helper.EncodePassword.HashPassword(string.Concat(userDTO.Password, user.Salting));
            user.Age = userDTO.Age;
            user.Name = userDTO.Name;
            _context.Users.Add(user);
            var check = _context.SaveChanges();
            if(check > 0)
            {
                return "Create successful";
            }
            return "Create fail";
        }

        [HttpPost]
        public string Login([FromBody] UserLoginDTO userLoginDTO)
        {
            var loginUser = _context.Users.SingleOrDefault(user => user.UserName.Equals(userLoginDTO.UserName));
            if (loginUser is not null)
            {
                var salting = loginUser.Salting;
                var password = Helper.EncodePassword.HashPassword(string.Concat(userLoginDTO.Password, salting));
                if(loginUser.Password.Equals(password))
                {
                    return "Login successful";
                }
                return "Wrong Password";
            }
            return "Wrong Username";
        }

        [HttpPut("{id}")]
        public string ForgetPassword(int id, [FromBody] UserForgetPasswordDTO userForgetPasswordDTO)
        {
            var user = _context.Users.SingleOrDefault(user => user.ID == id);
            if (user is not null)
            {
                user.Salting = Helper.EncodePassword.RandomSalt();
                user.Password = Helper.EncodePassword.HashPassword(userForgetPasswordDTO.Password);
                _context.Users.Update(user);
                _context.SaveChanges();
                return "Update successful";
            }
            return "Update fail";
        }
    }
}
