﻿using DI_TodoTaskAPI.DataAccess;
using DI_TodoTaskAPI.Model;
using Microsoft.AspNetCore.Mvc;
using DI_TodoTaskAPI.DTO.TodoDTO;
namespace DI_TodoTaskAPI.Controller
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TodoController : ControllerBase
    {
        private readonly AppDbContext _context;
        private static List<TodoTask> todoTasks = new List<TodoTask>();
        private List<TodoGetDTO> todoGetDTOs = new List<TodoGetDTO>();
        public TodoController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<TodoGetDTO> GetAll()
        {
            todoTasks = _context.TodoTasks.ToList();
            foreach (var todoTask in todoTasks)
            {
                TodoGetDTO todoGetDTO = new TodoGetDTO();
                todoGetDTO.Id = todoTask.Id;
                todoGetDTO.Content = todoTask.Content;
                todoGetDTO.IsCompleted = todoTask.IsCompleted;
                todoGetDTO.UserID = todoTask.UserID;

                todoGetDTOs.Add(todoGetDTO);
            }
            return todoGetDTOs;
        }

        [HttpGet("{id}")]
        public TodoGetDTO GetById(int id)
        {
            var todo = todoGetDTOs.SingleOrDefault(todo => todo.Id == id);
            if (todo is null)
            {
                throw new Exception("Does not exist");
            }
            return todo;
        }

        [HttpPost]
        public TodoTask CreateTodo([FromBody] TodoCreateDTO todoCreateDTO)
        {
            TodoTask todo = new TodoTask();
            todo.UserID = todoCreateDTO.UserID;
            todo.Content = todoCreateDTO.Content;
            todoTasks.Add(todo);
            _context.TodoTasks.Add(todo);
            _context.SaveChanges();
            return todo;
        }
        [HttpPut("{id}")]
        public string CompletedTask(int id)
        {
            var todo = todoTasks.SingleOrDefault(todo => todo.Id == id);
            if (todo is not null)
            {
                todo.IsCompleted = true;
                _context.TodoTasks.Update(todo);
                _context.SaveChanges();
                return "Update successful";
            }
            return "Update fail";
        }
        [HttpDelete("{id}")]
        public string DeleteTask(int id)
        {
            var todo = todoTasks.SingleOrDefault( todo => todo.Id == id);
            if(todo is not null)
            {
                _context.TodoTasks.Remove(todo);
                _context.SaveChanges();
                return "Remove successful";
            }
            return "Remove fail";
        }
    }
}
